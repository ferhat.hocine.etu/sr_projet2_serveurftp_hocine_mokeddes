package sr.projet2;

import java.io.IOException;

import sr.projet2.Server.ConnectionServer;

public class Main {
	/**
	 * la classe qui nous permet de lancer notre application
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException{
		ConnectionServer server=new ConnectionServer(2020);
		server.serverConnect();
		
	}
}
