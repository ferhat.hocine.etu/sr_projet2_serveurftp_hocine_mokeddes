package sr.projet2.Server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

import sr.projet2.Request.AUTH_SSL;
import sr.projet2.Request.AUTH_TLS;
import sr.projet2.Request.CDUP;
import sr.projet2.Request.CWD;
import sr.projet2.Request.FEAT;
import sr.projet2.Request.LIST;
import sr.projet2.Request.MKD;
import sr.projet2.Request.PASS;
import sr.projet2.Request.PASV;
import sr.projet2.Request.PWD;
import sr.projet2.Request.QUIT;
import sr.projet2.Request.RNFR;
import sr.projet2.Request.RNTO;
import sr.projet2.Request.SYST;
import sr.projet2.Request.TYPE;
import sr.projet2.Request.USER;

public class FtpConnexion extends Thread {
	/**
	 * établir une connexion ftp 
	 */
	private Socket socket;
	private BufferedReader bfR;
	private BufferedWriter bfW;
	private boolean Connected;
	private static String directory;
	public static ServerSocket socketServer;
	public static String rename;
	public FtpConnexion(Socket socket) throws IOException {
		this.socket=socket;
		this.bfR =new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
		this.bfW = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()));
		this.Connected=false;
		this.directory="/";
	}
	/**
	 * 
	 * @throws IOException
	 */
	public void auth() throws  IOException{
		try {
			this.bfW.write("220 FTP SERVER\r\n");
			this.bfW.flush();
		}catch(IOException e) {
			throw new IOException("FTP Connexion failed");
		}
	}
	/**
	 * 
	 */
	public void run() {
		try {
			this.auth();
		} catch (IOException e) {
			e.printStackTrace();
		}
		while(true) {
			try {
				String Read = this.bfR.readLine();
				this.read(Read);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * appliquer la méthode send d'une commande selon son type
	 * @param readLine
	 * @throws IOException
	 */
	public void read(String readLine) throws IOException {
		if(readLine.startsWith("AUTH TLS")) {
			AUTH_TLS request= new AUTH_TLS(this);
			request.send();
		}
		else if (readLine.startsWith("AUTH SSL")){
			AUTH_SSL request= new AUTH_SSL(this);
			request.send();
		}
		else if (readLine.startsWith("USER")){
			String[] mystring=readLine.split(" ");
			USER request= new USER(this,mystring[1].trim());
			request.send();
		}
		else if (readLine.startsWith("PASS")){
			String[] mystring=readLine.split(" ");
			PASS request= new PASS(this,mystring[1].trim());
			request.send();
		}
		else {
			if(this.Connected) {
				if(readLine.startsWith("PWD")) {
					PWD request= new PWD(this);
					request.send();
				}
				else if(readLine.startsWith("SYST")) {
					SYST request= new SYST(this);
					request.send();
				}
				else if(readLine.startsWith("FEAT")) {
					FEAT request= new FEAT(this);
					request.send();
				}
				else if (readLine.startsWith("TYPE")){
					String[] mystring=readLine.split(" ");
					TYPE request= new TYPE(this,mystring[1].trim());
					request.send();
				}
				else if (readLine.startsWith("PASV")){
					PASV request= new PASV(this);
					request.send();
				}
				else if (readLine.startsWith("LIST")){
					LIST request= new LIST(this);
					request.send();
				}
				else if (readLine.startsWith("CWD")){
					String[] mystring=readLine.split(" ");
					CWD request= new CWD(this,mystring[1].trim());
					request.send();
				}
				else if (readLine.startsWith("CDUP")){
					CDUP request= new CDUP(this);
					request.send();
				}
				else if (readLine.startsWith("MKD")){
					String[] mystring=readLine.split(" ");
					MKD request= new MKD(this,mystring[1].trim());
					request.send();
				}
				else if (readLine.startsWith("RNFR")){
					String[] mystring=readLine.split(" ");
					RNFR request= new RNFR(this,mystring[1].trim());
					request.send();
				}
				else if (readLine.startsWith("RNTO")){
					String[] mystring=readLine.split(" ");
					RNTO request= new RNTO(this,mystring[1].trim());
					request.send();
				}
				else if (readLine.startsWith("QUIT")){
					QUIT request= new QUIT(this);
					request.send();
				}
			}
			else {
				this.bfW.write("530 server is not connected\r\n");
				this.bfW.flush();
			}
		}

	}
	/**
	 * 
	 * @return socket
	 */
	public Socket getSocket() {
		return this.socket;
	}
	/**
	 * 
	 * @return bufferwriter
	 */
	public BufferedWriter getBufferedWriter() {
		return this.bfW;
	}
	/**
	 * 
	 * @return bufferReader
	 */
	public BufferedReader getBufferedReader() {
		return this.bfR;
	}
	/**
	 * 
	 * @return connexionstate
	 */
	public boolean getConnectedState(){
		return this.Connected;
	}
	/**
	 *  connexion 
	 */
	public void connect() {
		this.Connected=true;
	}
	/**
	 * deconnexion
	 */
	public void disconnect(){
		this.Connected=false;
	}
	/**
	 * 
	 * @return rep
	 */
	public String getDirectory(){
		return this.directory;
	}
	/**
	 * modifier le repertoir ou l'initialiser
	 * @param Directory
	 */
	public void SetDirectory(String Directory){
		this.directory=Directory;
	}
	/**
	 * 
	 * @return socketserver
	 */
	public ServerSocket getServerSocket(){
		return this.socketServer;
	}
	/**
	 * 
	 * @param s
	 */
	public void SetServerSocket(ServerSocket s){
		this.socketServer=s;
	}
	/**
	 * 
	 * @return rename
	 */
	public String getRename(){
		return this.rename;
	}
	/**
	 * rename
	 * @param s
	 */
	public void SetRename(String s){
		this.rename=s;
	}
}
