package sr.projet2.Server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class ConnectionServer {
	/**
	 * connexion au serveur
	 */
	private int PORT;
	private ServerSocket srvSckt;
	
	/**
	 * verifie si le port est bon , sinon on va generer un autre aléatoirment 
	 * @param PORT
	 * @throws IOException
	 */
	public ConnectionServer (int PORT) throws IOException {
		if(PORT>1024 && PORT<8080){
		this.PORT=PORT;
		}
		else {
			Random rand = new Random(); 
			int nombreAleatoire = rand.nextInt(8079 - 1025) + 1024;
			this.PORT=nombreAleatoire;
		}
		server();
		
	}
	
	/**
	 * 
	 * @throws IOException
	 */
	public void server() throws IOException {
		try {
		this.srvSckt=new ServerSocket(this.PORT);
		System.out.println("server started");
		}
		catch (IOException e) {
			throw new IOException("Connexion failed");
		}
		
	}
	/**
	 * 
	 * @return serveSocket
	 */
	public ServerSocket getServ() {
		return this.srvSckt;
	}
	/**
	 * 
	 * @throws IOException
	 */
	public void serverConnect() throws IOException {
		try {
		Socket socket=this.srvSckt.accept();
		FtpConnexion ftp= new FtpConnexion(socket);
		ftp.run();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
	}

}
