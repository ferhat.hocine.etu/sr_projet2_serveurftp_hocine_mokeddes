package sr.projet2.Request;

import java.io.File;
import java.io.IOException;

import sr.projet2.Server.FtpConnexion;

public class MKD implements request {
	/**  
     * Process pour creer un dossier
     * [MKD]
     */
	
	private FtpConnexion ftp;
	private String DIR;
	public MKD(FtpConnexion ftp, String DIR) {
		this.ftp =ftp;
		this.DIR=DIR;
	}

	@Override
	public void send() throws IOException{
		if (this.ftp.getSocket() == null ) {
			throw new IOException("Ftp server error");

		}
		try {
			if(!this.DIR.equals("")) {
				File directory=new File(this.ftp.getDirectory());
				if(directory.canWrite() || directory.canExecute() ) {
					File directory1=new File(this.ftp.getDirectory()+"/"+this.DIR);
					if (directory1.exists()) {
						this.ftp.getBufferedWriter().write("550 already existe.\r\n");
						this.ftp.getBufferedWriter().flush();
					}
					else {
					if(directory1.mkdir()) {
					this.ftp.getBufferedWriter().write("250 directory created.\r\n");
					this.ftp.getBufferedWriter().flush();
					}
					else {
						this.ftp.getBufferedWriter().write("550 rights error.\r\n");
						this.ftp.getBufferedWriter().flush();
					}
					}
				}
				else {
					this.ftp.getBufferedWriter().write("550 rights error.\r\n");
					this.ftp.getBufferedWriter().flush();
				}
			}
		}
		catch(IOException e){
			throw new IOException("Connexion failed");
		}
	}

}
