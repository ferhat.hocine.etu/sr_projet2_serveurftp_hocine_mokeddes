package sr.projet2.Request;

import java.io.IOException;

import sr.projet2.Server.FtpConnexion;

public class TYPE implements request {
	/**
	 * return mode type
	 * [TYPE]
	 */
	private FtpConnexion ftp;
	private String type;
	public TYPE(FtpConnexion ftp,String type) {
		this.ftp =ftp;
		this.type=type;
	}

	@Override
	public void send() throws IOException{
		if (this.ftp.getSocket() == null ) {
			throw new IOException("Ftp server error");
		}
		try {
	    	switch(this.type) {
	    		case "A" : 
	    			this.type = "ASCII";
	    			break;
	    		case "E" : 
	    			this.type = "EBCDIC"; 
	    			break;
	    		case "I" : 
	    			this.type = "Switching to Binary mode"; 
	    			break;
	    		case "L" : 
	    			this.type = "local"; 
	    			break;
	    		default: 
	    			this.ftp.getBufferedWriter().write("400 incorrect type.\r\n"); 
	    			this.ftp.getBufferedWriter().flush();
	    			return;
	    	}
			this.ftp.getBufferedWriter().write("200 Type "+this.type+".\r\n");
			this.ftp.getBufferedWriter().flush();
		}
		catch(IOException e){
			throw new IOException("Connexion failed");
		}
	}

}
