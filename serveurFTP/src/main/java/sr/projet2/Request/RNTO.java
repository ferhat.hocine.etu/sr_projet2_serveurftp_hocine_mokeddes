package sr.projet2.Request;

import java.io.File;
import java.io.IOException;

import sr.projet2.Server.FtpConnexion;

public class RNTO implements request {
	
    /**
     * Process rename to ( renomer le dossier sauvgardé avec rnfr )
     * [RNTO]
     */
	private FtpConnexion ftp;
	private String name;
	public RNTO(FtpConnexion ftp, String name) {
		this.ftp =ftp;
		this.name=name;
	}

	@Override
	public void send() throws IOException{
		if (this.ftp.getSocket() == null ) {
			throw new IOException("Ftp server error");

		}
		try {
			File file= new File(this.ftp.getRename());
			File file2= new File(this.ftp.getDirectory()+"/"+this.name);

			if(file.renameTo(file2)){
				this.ftp.getBufferedWriter().write("250 file '"+this.ftp.getRename()+"' renamed to '"+this.ftp.getDirectory()+"/"+this.name+"'.\r\n");
				this.ftp.getBufferedWriter().flush();
			}
			else {
				this.ftp.getBufferedWriter().write("553 not found.\r\n");
				this.ftp.getBufferedWriter().flush();
			}
			}

		catch(IOException e){
			throw new IOException("Connexion failed");
		}
	}

}
