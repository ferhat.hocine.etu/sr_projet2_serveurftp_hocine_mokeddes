package sr.projet2.Request;

import java.io.IOException;

import sr.projet2.Server.FtpConnexion;

public class QUIT implements request {
	/**
	 * process pour fermer un socket
	 * [QUIT]
	 */
	private FtpConnexion ftp;
	public QUIT(FtpConnexion ftp) {
		this.ftp =ftp;
	}

	@Override
	public void send() throws IOException{
		if (this.ftp.getSocket() == null ) {
			throw new IOException("Ftp server error");

		}
		try {
				this.ftp.getBufferedWriter().write("232 Quit.\r\n");
				this.ftp.getBufferedWriter().flush();
				this.ftp.getSocket().close();
				this.ftp.disconnect();
				System.out.println("Quit");
		}
		catch(IOException e){
			this.ftp.connect();
			throw new IOException("Connexion failed");
		}
	}

}
