package sr.projet2.Request;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.nio.file.Files;
import java.sql.Date;
import java.text.SimpleDateFormat;

import sr.projet2.Server.FtpConnexion;

public class LIST implements request {
	/**
	 *     
     * Process  qui permet de lister un repertoir
     * [LIST]
     */
	 
	private FtpConnexion ftp;
	public LIST(FtpConnexion ftp) {
		this.ftp =ftp;
	}

	@Override
	public void send() throws IOException{
		if (this.ftp.getSocket() == null ) {
			throw new IOException("Ftp server error");
		}
		try {
			File file=new File(this.ftp.getDirectory());
			if(file.isDirectory()) {
				this.ftp.getBufferedWriter().write("150 Here comes the directory listing.\r\n");
				this.ftp.getBufferedWriter().flush();
				File[] files = file.listFiles();
				Socket socket1=this.ftp.getServerSocket().accept();
				BufferedReader bfR1 =new BufferedReader(new InputStreamReader(socket1.getInputStream()));
				BufferedWriter bfW1 = new BufferedWriter(new OutputStreamWriter(socket1.getOutputStream()));
				String res="";
				for(File oneFile: files) {
					int len =1;
					if(oneFile.isDirectory() && oneFile.canRead()) {
						res+="d"; // if directory
						len=oneFile.listFiles().length;
					}
					else {
						res+="-";
					}
					res+="rwxr-xr-x		"; //Permission
					res+=len+" ";
					//res+= Files.getOwner(oneFile.toPath()).toString()+"	";
		    		res+= "root		 ";
					Date date = new Date(oneFile.lastModified());
		            SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd hh:mm");
		            res+= dateFormat.format(date)+" ";
		            res+=oneFile.getName()+"\r\n";

				}
	            bfW1.write(res);
	            bfW1.flush();
				socket1.close();
				this.ftp.getBufferedWriter().write("226 Directory send OK.\r\n");
				this.ftp.getBufferedWriter().flush();
			}
		}
		catch(IOException e){
			throw new IOException("Connexion failed");
		}
	}

}
