package sr.projet2.Request;

import java.io.IOException;

import sr.projet2.Server.FtpConnexion;

public class AUTH_SSL implements request {
	/**
	 * authentification de notre application
	 */
	private FtpConnexion ftp;
	public AUTH_SSL(FtpConnexion ftp) {
		this.ftp =ftp;
	}

	
	/*
	 * 
	 */
	public void send() throws IOException{
		if (this.ftp.getSocket() == null ) {
			throw new IOException("Ftp server error");
		}
		try {
			this.ftp.getBufferedWriter().write("530 please login with USER AND PASS.\r\n");
			this.ftp.getBufferedWriter().flush();
		}
		catch(IOException e){
			throw new IOException("Connexion failed");
		}
	}

}
