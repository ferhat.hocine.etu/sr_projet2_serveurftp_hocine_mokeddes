package sr.projet2.Request;

import java.io.File;
import java.io.IOException;

import sr.projet2.Server.FtpConnexion;

public class CDUP implements request {
	/**
	 * retour au dossier parent
     * Process pour retourner au dossier parent
     * [CDUP]
	 */
	private FtpConnexion ftp;
	public CDUP(FtpConnexion ftp) {
		this.ftp =ftp;
	}

	@Override
	public void send() throws IOException{
		if (this.ftp.getSocket() == null ) {
			throw new IOException("Ftp server error");
		}
		try {
			String newPath=this.ftp.getDirectory();
			String[] pa=newPath.split("/");
			newPath="";
			if(pa.length>2) {
			for(int i=1;i<pa.length-1;i++) {
				newPath+="/"+pa[i];
			}
			}
			else {
				newPath="/";
			}
			this.ftp.SetDirectory(newPath);
			this.ftp.getBufferedWriter().write("250 success to exit.\r\n");
			this.ftp.getBufferedWriter().flush();
		}
		catch(IOException e){
			throw new IOException("Connexion failed");
		}
	}

}
