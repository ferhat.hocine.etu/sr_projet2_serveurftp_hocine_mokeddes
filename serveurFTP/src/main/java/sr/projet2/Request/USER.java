package sr.projet2.Request;

import java.io.IOException;

import sr.projet2.Server.FtpConnexion;

public class USER implements request {
	/**
	 * process pour l'authentification -> utilisateur
	 * [USER]
	 */
	private FtpConnexion ftp;
	private String user;
	public USER(FtpConnexion ftp,String user) {
		this.ftp =ftp;
		this.user=user;
	}

	@Override
	public void send() throws IOException{
		if (this.ftp.getSocket() == null ) {
			throw new IOException("Ftp server error");
		}
		try {
			if (this.user.equals("anonymous")){
				this.ftp.getBufferedWriter().write("331 PLEASE SPECIFY THE PASSWORD\r\n");
				this.ftp.getBufferedWriter().flush();
			}
			else{
				this.ftp.getBufferedWriter().write("530 unknown User\r\n");
				this.ftp.getBufferedWriter().flush();
			}
		}
		catch(IOException e){
			throw new IOException("Connexion failed");
		}
	}

}
