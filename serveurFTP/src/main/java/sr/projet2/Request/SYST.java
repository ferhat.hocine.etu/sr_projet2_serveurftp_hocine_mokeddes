package sr.projet2.Request;

import java.io.IOException;

import sr.projet2.Server.FtpConnexion;

public class SYST implements request {
    /**
     * Process rename to ( renomer le dossier sauvgardé avec rnfr )
     * [SYST]
     */
	private FtpConnexion ftp;
	public SYST(FtpConnexion ftp) {
		this.ftp =ftp;
	}

	@Override
	public void send() throws IOException{
		if (this.ftp.getSocket() == null ) {
			throw new IOException("Ftp server error");
		}
		try {
			this.ftp.getBufferedWriter().write("215 UNIX Type: L8.\r\n");
			this.ftp.getBufferedWriter().flush();
		}
		catch(IOException e){
			throw new IOException("Connexion failed");
		}
	}

}
