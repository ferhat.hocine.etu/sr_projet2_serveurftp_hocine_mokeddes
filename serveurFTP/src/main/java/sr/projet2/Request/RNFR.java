package sr.projet2.Request;

import java.io.File;
import java.io.IOException;

import sr.projet2.Server.FtpConnexion;

public class RNFR implements request {
    /**
     * Process rename from pour sauvgarder le nom du dossier a renomer
     * [RNFR]
     */
	private FtpConnexion ftp;
	private String name;
	public RNFR(FtpConnexion ftp, String name) {
		this.ftp =ftp;
		this.name=name;
	}

	@Override
	public void send() throws IOException{
		if (this.ftp.getSocket() == null ) {
			throw new IOException("Ftp server error");

		}
		try {
			File file= new File(this.ftp.getDirectory()+"/"+this.name);
			if(file.exists()) {
				this.ftp.SetRename(this.ftp.getDirectory()+"/"+this.name);
				this.ftp.getBufferedWriter().write("250 found.\r\n");
				this.ftp.getBufferedWriter().flush();
			}
			else {
				this.ftp.getBufferedWriter().write("553 not found.\r\n");
				this.ftp.getBufferedWriter().flush();
			}
			}

		catch(IOException e){
			throw new IOException("Connexion failed");
		}
	}

}
