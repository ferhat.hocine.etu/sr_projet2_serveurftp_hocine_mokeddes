package sr.projet2.Request;

import java.io.IOException;

import sr.projet2.Server.FtpConnexion;

public class PWD implements request {
    /**
     * Process pour renvoyer l'enplacement courant (le chemin)
     * [PWD]
     */
	private FtpConnexion ftp;
	public PWD(FtpConnexion ftp) {
		this.ftp =ftp;
	}

	@Override
	public void send() throws IOException{
		if (this.ftp.getSocket() == null ) {
			throw new IOException("Ftp server error");
		}
		try {
			
			this.ftp.getBufferedWriter().write("257 \""+this.ftp.getDirectory()+"\" is the current directory.\r\n");
			this.ftp.getBufferedWriter().flush();
		}
		catch(IOException e){
			throw new IOException("Connexion failed");
		}
	}

}
