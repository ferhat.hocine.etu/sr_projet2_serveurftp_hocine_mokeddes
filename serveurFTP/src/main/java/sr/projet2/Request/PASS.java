package sr.projet2.Request;

import java.io.IOException;

import sr.projet2.Server.FtpConnexion;

public class PASS implements request {
	/**
	 * procces pour l'authentification -> mot de passe
	 * [PASS]
	 */
	private FtpConnexion ftp;
	private String pass;
	public PASS(FtpConnexion ftp, String pass) {
		this.ftp =ftp;
		this.pass=pass;
	}

	@Override
	public void send() throws IOException{
		if (this.ftp.getSocket() == null ) {
			throw new IOException("Ftp server error");

		}
		try {
			if (this.pass.equals("anonymous")){
				this.ftp.getBufferedWriter().write("230 Login successful.\r\n");
				this.ftp.getBufferedWriter().flush();
				this.ftp.connect();
			}else{
				this.ftp.getBufferedWriter().write("530 unknown Password\r\n");
				this.ftp.getBufferedWriter().flush();
				}
		}
		catch(IOException e){
			throw new IOException("Connexion failed");
		}
	}

}
