package sr.projet2.Request;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import sr.projet2.Server.FtpConnexion;

public class PASV implements request {
    /**
     * Process pour passer en mode passif
     * [PASV]
     */
	private FtpConnexion ftp;
	public PASV(FtpConnexion ftp) {
		this.ftp =ftp;
	}

	@Override
	public void send() throws IOException{
		if (this.ftp.getSocket() == null ) {
			throw new IOException("Ftp server error");
		}
		try {
			ServerSocket s=new  ServerSocket(0);
			this.ftp.SetServerSocket(s);
            int newPort = this.ftp.getServerSocket().getLocalPort();
            int port1 = newPort / 256;
    		int port2 = newPort % 256;
    		String ips[] = this.ftp.getSocket().getLocalAddress().getHostAddress().split("\\.");
    		String ip = ips[0] + "," + ips[1] + "," + ips[2] + "," + ips[3] + "," + port1 + "," + port2;
			this.ftp.getBufferedWriter().write("227 Entering Passive Mode ("+ip+").\r\n");
			this.ftp.getBufferedWriter().flush();
		}
		catch(IOException e){
			throw new IOException("Connexion failed");
		}
	}

}
