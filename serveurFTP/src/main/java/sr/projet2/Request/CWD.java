package sr.projet2.Request;

import java.io.File;
import java.io.IOException;

import sr.projet2.Server.FtpConnexion;

public class CWD implements request {
	/**
	 * 
     * Process pour ouvrir un dossier
     * [CWD]
     */
	 
	private FtpConnexion ftp;
	private String directory;
	public CWD(FtpConnexion ftp,String directory) {
		this.ftp =ftp;
		this.directory=directory;
	}

	@Override
	public void send() throws IOException{
		if (this.ftp.getSocket() == null ) {
			throw new IOException("Ftp server error");
		}
		try {
			String newPath;
			if(this.ftp.getDirectory().trim().equals("/")) {
				newPath=this.ftp.getDirectory()+this.directory;
			}
			else {
				newPath=this.ftp.getDirectory()+"/"+this.directory;

			}
			File file= new File(newPath);
			if (file.exists() && file.canRead() && file.isDirectory()) {
				this.ftp.SetDirectory(newPath);
				this.ftp.getBufferedWriter().write("200 success to access to the directory.\r\n");
				this.ftp.getBufferedWriter().flush();
			}
			else {
				this.ftp.getBufferedWriter().write("550 failed to access to the directory. \r\n");
				this.ftp.getBufferedWriter().flush();
			}
		}
		catch(IOException e){
			throw new IOException("Connexion failed");
		}
	}

}
